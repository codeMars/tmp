package com.mars.springboot.common.utils;

import org.apache.commons.lang.StringUtils;

public class CommonUtil {


    //拼接为Sql查询的字符串：'a','b','c' 或者 'a,b,c'
    public static String concatStrToChar(String parm,String seqaratorChars){
        StringBuilder sb = new StringBuilder();
        String[] str = StringUtils.split(parm,seqaratorChars);
        int i = 0;
        for(;i<str.length;i++){
            if(str.length-1 != i ){
                sb.append("'").append(str[i]).append("'").append(",");
            }else {
                sb.append("'").append(str[i]).append("'");
            }
        }
        return sb.toString();
    }

    /**
     * 分隔后拼接成用于sql查询的字符串- a,b,c
     * @param param
     * @param separatorChars
     * @return
     */
    public static String concatStrToInt(String param, String separatorChars){
        StringBuilder sb=new StringBuilder();
        String[] arr= StringUtils.split(param,separatorChars);
        int i=0;
        for (;i<arr.length;i++){
            if (arr.length-1 != i){
                sb.append(arr[i]).append(",");
            }else{
                sb.append(arr[i]);
            }
        }
        return sb.toString();
    }
}
