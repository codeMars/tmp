package com.mars.springboot.common.utils;


import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

//分页的工具类

@Data
public class PageUtil implements Serializable {

    private long totalCount;//总记录数
    private long totalPage; //总页数
    private long currPage; //当前页数
    private long pageSize; //每页显示记录数
    private List<?> list; //数据列表

    public PageUtil(long totalCount, long totalPage, long currPage, long pageSize, List<?> list) {
        this.totalCount = totalCount;
        this.totalPage = totalPage;
        this.currPage = currPage;
        this.pageSize = pageSize;
        this.list = list;
    }

    public PageUtil(IPage<?> page){
        this.totalCount = page.getTotal();
        this.totalPage = page.getPages();
        this.currPage = page.getCurrent();
        this.pageSize = page.getSize();
        this.list = page.getRecords();
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }

    public long getCurrPage() {
        return currPage;
    }

    public void setCurrPage(long currPage) {
        this.currPage = currPage;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "PageUtil{" +
                "totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", currPage=" + currPage +
                ", pageSize=" + pageSize +
                ", list=" + list +
                '}';
    }
}
