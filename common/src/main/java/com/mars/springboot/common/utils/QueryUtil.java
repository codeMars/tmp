package com.mars.springboot.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import java.util.Map;

public class QueryUtil<T> {


    //重载的查询
    public IPage<T> getQueryPage(Map<String,Object> paraMap){
        //当前第几页，每页显示多少条
        long curPage = 1;
        long limit = 10;

        if(paraMap.get(Constant.PAGE) != null){
            curPage = Long.valueOf(paraMap.get(Constant.PAGE).toString());
        }
        if(paraMap.get(Constant.LIMIT) != null){
            limit = Long.valueOf(paraMap.get(Constant.LIMIT).toString());
        }

        //分页对象
        Page<T> page = new Page<>(curPage,limit);

        //设置前端请求的字段排序
        if(paraMap.get(Constant.ORDER) != null && paraMap.get(Constant.ORDER_FIELD) != null ){
            if(Constant.ASC.equalsIgnoreCase(paraMap.get(Constant.ORDER).toString())){
                return page.setAsc(paraMap.get(Constant.ORDER_FIELD).toString());
            }else {
                return page.setDesc(paraMap.get(Constant.ORDER_FIELD).toString());

            }
        }

        return page;
    }

}
