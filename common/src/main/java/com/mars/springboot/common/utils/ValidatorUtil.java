package com.mars.springboot.common.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.function.Consumer;

/*
*  请求参数统一校验工具
* */
public class ValidatorUtil {

    //统一处理校验结果
    public static String checkResult(BindingResult result){

        StringBuilder sb = new StringBuilder("");
        if(result != null && result.hasErrors()){
        // 方法1
/*            List<ObjectError> errorList = result.getAllErrors();
            for(ObjectError error : errorList){
                sb.append(error.getDefaultMessage()).append("\n");
            }*/

        //方法2
            result.getAllErrors().stream().forEach(error -> sb.append(error.getDefaultMessage()).append("\n"));

        }
        return sb.toString();
    }

}
