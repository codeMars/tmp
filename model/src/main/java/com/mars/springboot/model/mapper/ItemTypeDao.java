package com.mars.springboot.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.model.entity.ItemTypeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator on 2019/7/23.
 */
@Mapper
public interface ItemTypeDao extends BaseMapper<ItemTypeEntity>{


}
