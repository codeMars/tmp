package com.mars.springboot.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.model.entity.SysDeptEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

//部门管理
@Mapper
public interface SysDeptDao extends BaseMapper<SysDeptEntity> {

    List<SysDeptEntity> queryList(Map<String, Object> params);

    List<SysDeptEntity> queryListV1();

    List<SysDeptEntity> queryListV2(Map<String, Object> params);

    List<SysDeptEntity> queryDeptAllList(Map<String, Object> params);


    //根据父级部门id查询子部门id列表
    List<Long> queryDeptIds(Long parentId);

    //超级管理员--查询所有信息-无权限控制
    Set<Long> queryAllDeptIds();
}
