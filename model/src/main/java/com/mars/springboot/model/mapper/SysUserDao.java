package com.mars.springboot.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.model.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

//系统用户
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {
	
	//查询用户的所有权限
	List<String> queryAllPerms(Long userId);

	//查询用户的所有菜单
	List<Long> queryAllMenuId(Long userId);


	//根据用户id获取部门数据Id列表 ~ 数据权限
	Set<Long> queryDeptIdsByUserId(Long userId);

	SysUserEntity selectByUserName(@Param("userName") String userName);

	//更新密码
	void updatePassword2(Long userId,String oldPassword,String newPassword);

}
