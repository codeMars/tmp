package com.mars.springboot.server.controller;


import com.google.common.collect.Maps;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.server.aspect.LogAspect;
import com.mars.springboot.server.service.SysLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/sys/log")
public class SysLogController extends AbstractController {

    private static final Logger logger = LoggerFactory.getLogger(SysLogController.class);

    @Autowired
    private SysLogService sysLogService;

    //查询
    @GetMapping("/list")
    public BaseResponse list(@RequestParam Map<String,Object> params){

        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();
        try{
            logger.info("日志模块-list查询");
            PageUtil page = sysLogService.queryPage(params);

        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //删除
    @GetMapping("/truncate")
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse truncate(){
        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();
        try{
            logger.info("日志模块删除操作");
            sysLogService.truncate();

        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }


}
