package com.mars.springboot.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.model.entity.SysPostEntity;
import com.mars.springboot.model.mapper.SysPostDao;

import java.util.Map;

//使用mp的方式
public interface SysPostService extends IService<SysPostEntity> {

    PageUtil queryPage(Map<String,Object> paraMap);
    void savePost(SysPostEntity entity);
    void updatePost(SysPostEntity entity);
    void deletePach(Long[] ids);


}
