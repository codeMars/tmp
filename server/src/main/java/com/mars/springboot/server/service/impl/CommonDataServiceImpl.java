package com.mars.springboot.server.service.impl;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import com.mars.springboot.common.utils.CommonUtil;
import com.mars.springboot.common.utils.Constant;
import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.model.mapper.SysDeptDao;
import com.mars.springboot.model.mapper.SysUserDao;
import com.mars.springboot.server.service.CommonDataService;
import com.mars.springboot.server.service.SysDeptService;
import com.mars.springboot.server.util.ShiroUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service("commonDataService")
public class CommonDataServiceImpl implements CommonDataService {

    private static final Logger logger = LoggerFactory.getLogger(CommonDataServiceImpl.class);

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private SysDeptDao sysDeptDao;

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public Set<Long> getCurrUserDataDeptIds() {
        Set<Long> dataIds = Sets.newHashSet();

        SysUserEntity currUser  = ShiroUtil.getUserEntity();

        if(currUser.getUserId() == Constant.SUPER_ADMIN){
            dataIds= sysDeptDao.queryAllDeptIds();
        }else {
            //分配给用户的部门数据权限id列表
            //		SELECT roleDept.dept_id
            //		FROM sys_role_dept AS roleDept
            //		  LEFT JOIN sys_user_role AS userRole ON userRole.role_id = roleDept.role_id
            //		WHERE userRole.user_id = #{userId}

            //查询当前用户所在部门
            Set<Long> userDeptIds = sysUserDao.queryDeptIdsByUserId(currUser.getUserId());
            logger.info("userDeptIds:{}",userDeptIds);
            if(userDeptIds != null && !userDeptIds.isEmpty()){
                dataIds.addAll(userDeptIds);
                logger.info("add_userDeptIds:{}",userDeptIds);
            }
            //用户所在部门及其子部门id列表～递归实现
            dataIds.add(currUser.getDeptId());  //如果非超级用户，查询结果为null

           //上面查出来的是从集团开始。。

//         //下面查出来的是当前登录用户下一级的目录
//         List<Long> subDeptIdList = sysDeptService.getSubDeptIdsList(currUser.getDeptId());
//         dataIds.addAll(Sets.newHashSet(subDeptIdList));


        }
        return dataIds;
    }

    @Override
    public String getCurrUserDataDeptIdsStr() {
        String result = null;
        Set<Long> dataSet =this.getCurrUserDataDeptIds();
        if(dataSet != null && !dataSet.isEmpty()){
            result=CommonUtil.concatStrToInt(Joiner.on(",").join(dataSet),",");
        }


        return result;
    }
}















