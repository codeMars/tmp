package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.mars.springboot.common.utils.CommonUtil;
import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.model.entity.SysUserRoleEntity;
import com.mars.springboot.model.mapper.SysUserRoleDao;
import com.mars.springboot.server.service.SysUserRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("sysUserRoleService")
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleDao, SysUserRoleEntity> implements SysUserRoleService {

    private static final Logger log = LoggerFactory.getLogger(SysUserRoleServiceImpl.class);


    @Override
    public void deleteBatch(List<Long> roleIdList) {
        if(roleIdList != null && !roleIdList.isEmpty()){
            String delIds = Joiner.on(",").join(roleIdList);
            baseMapper.deleteBatch(CommonUtil.concatStrToInt(delIds, ","));

        }
    }

    //维护用户和角色的关联关系
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(Long userId, List<Long> roleIdList) {

        //删除原有数据，删除userid删除关联关系
        this.remove(new QueryWrapper<SysUserRoleEntity>().eq("user_id",userId));

        //新增/保存用户和角色的关联关系
        if(!roleIdList.isEmpty() && roleIdList != null){
            SysUserRoleEntity userRoleEntity;
            for(Long rId : roleIdList){
                userRoleEntity = new SysUserRoleEntity();
                userRoleEntity.setRoleId(rId);
                userRoleEntity.setUserId(userId);
                this.save(userRoleEntity);
            }
        }
    }

    //获取分配给用户的角色列表
    @Override
    public List<Long> queryRoleIdList(Long userId) {

        return baseMapper.queryRoleIdList(userId);
    }
}
