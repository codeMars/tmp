package com.mars.springboot.server.aspect;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mars.springboot.common.utils.HttpContextUtils;
import com.mars.springboot.common.utils.IPUtil;
import com.mars.springboot.model.entity.SysLogEntity;
import com.mars.springboot.server.annotation.LogAnnotation;
import com.mars.springboot.server.controller.AbstractController;
import com.mars.springboot.server.service.SysLogService;
import com.mars.springboot.server.util.ShiroUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

//切面处理类
@Component
@Aspect
public class LogAspect {



    private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @Autowired
    private SysLogService sysLogService;

    @Pointcut("@annotation(com.mars.springboot.server.annotation.LogAnnotation)")
    public void logPointCut(){

    }

    //环绕通知
    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable{

        long startTime = System.currentTimeMillis();
        Object object = point.proceed();
        long execTime = System.currentTimeMillis() - startTime;
        saveLog(point,execTime);
        return  object;
    }

    //保存日志
    private void saveLog(ProceedingJoinPoint point,Long execTime){
        MethodSignature signature = (MethodSignature)point.getSignature();
        Method method= signature.getMethod();
        SysLogEntity sysLogEntity = new SysLogEntity();

        //获取请求操作的描述信息
        LogAnnotation logAnnotation = method.getAnnotation(LogAnnotation.class);
        if(logAnnotation !=null){
            //对应controller上面的"@LogAnnotation("修改登录密码")"的修改登录密码
            sysLogEntity.setOperation(logAnnotation.value());
        }

        //获取请求的操作方法
        String className = point.getTarget().getClass().getName(); //获取目标对象的类名
        String methodName = signature.getName(); //方法名
        //set 方法名
        sysLogEntity.setMethod(new StringBuilder().append(className).append(".").append(methodName).append("()").toString());

        //获取请求参数 ~~ 使用gson方式
        Object[] args = point.getArgs();
        logger.info("args:{}",JSONObject.toJSON(args));
        String param = JSON.toJSONString(args[0]);
        sysLogEntity.setParams(param);

        //获取IP
        sysLogEntity.setIp(IPUtil.getIpAddr(HttpContextUtils.getHttpServletRequest()));

        //获取剩下的信息
        sysLogEntity.setCreateDate(DateTime.now().toDate());
        String userName = ShiroUtil.getUserEntity().getUsername();
        sysLogEntity.setUsername(userName);
        sysLogEntity.setTime(execTime);


        sysLogService.save(sysLogEntity);



    }


}
