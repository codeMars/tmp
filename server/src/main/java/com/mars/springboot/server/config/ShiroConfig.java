package com.mars.springboot.server.config;

import com.mars.springboot.server.shiro.UserRealm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Administrator on 2019/10/27.
 */
@Configuration
public class ShiroConfig {

    //安全器管理-管理所有的subject
    @Bean
    public SecurityManager securityManager(UserRealm userRealm){
        DefaultWebSecurityManager securityManager =new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm);
        securityManager.setRememberMeManager(null);
        return securityManager;
    }

    //过滤链配置
    @Bean("shiroFilter")
    public ShiroFilterFactoryBean  shiroFilter(SecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        //设定用户没登录认证时跳转链接，没授权时跳转链接
        shiroFilter.setLoginUrl("/login.html");
        shiroFilter.setUnauthorizedUrl("/");

        //过滤链配置
        Map<String,String> filterMap = new LinkedHashMap();
        filterMap.put("/swagger/**","anon");
        filterMap.put("/v2/api-docs**","anon");
        filterMap.put("/swagger-ui.html","anon");
        filterMap.put("/webjars/**","anon");
        filterMap.put("/swagger-resources/**","anon");
        filterMap.put("/login.html","anon");
        filterMap.put("/sys/login","anon");
        filterMap.put("/favicon.ico","anon");
        filterMap.put("/captcha.jpg","anon");
        filterMap.put("/statics/**","anon");//访问静态资源，可以匿名直接访问


        //访问所有页面，都需要进行授权才能访问　
        filterMap.put("/**","authc");

        shiroFilter.setFilterChainDefinitionMap(filterMap);

        return shiroFilter;


    }


    //关于shiro的bean生命周期管理
    @Bean("lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor(){
        return new LifecycleBeanPostProcessor();
    }

    @Bean("authorizationAttributeSourceAdvisor")
    //返回报错
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }


}
