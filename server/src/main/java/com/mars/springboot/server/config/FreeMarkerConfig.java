package com.mars.springboot.server.config;


import com.mars.springboot.server.shiro.ShiroVariable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
public class FreeMarkerConfig {

    @Bean
    public FreeMarkerConfigurer freeMarkerConfigurer(ShiroVariable shiroVariable){

        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPath("classpath:/templates");
        Map<String,Object> variles = new HashMap<>(1);
        variles.put("shiro",shiroVariable);
        freeMarkerConfigurer.setFreemarkerVariables(variles);

        Properties settings = new Properties();
        settings.setProperty("default_encoding","utf-8");
        settings.setProperty("number_format","0.##");

        freeMarkerConfigurer.setFreemarkerSettings(settings);
        return freeMarkerConfigurer;
    }


}
