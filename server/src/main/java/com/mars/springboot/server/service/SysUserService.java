package com.mars.springboot.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.model.entity.SysUserEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface SysUserService extends IService<SysUserEntity> {

    //修改密码
    boolean updatePassword(Long userId,String oldPassword,String newPassword);


    //分页查询
    PageUtil queryPage(Map<String,Object> paraMap);

    void saveUser(SysUserEntity entity);


    SysUserEntity getInfo(Long userId);

    void updateUser(SysUserEntity entity);

    void deleteUser(Long[] userIds);

    void resetPsd(Long[] userIds);

}
