package com.mars.springboot.server.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.server.util.ShiroUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.shiro.subject.Subject;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

/**
 * 登录controller
 * Created by Administrator on 2019/10/27.
 */

@Controller
public class SysLoginController extends AbstractController {

    @Autowired
    private Producer producer;

    @GetMapping("/login.html")
    public String login(){
        if(SecurityUtils.getSubject().isAuthenticated()){
            return "redirect:index.html";
        }
        return "login";
    }

    @RequestMapping(value = {"/index.html",""})
    public String index(){
        return "index";
    }

    @GetMapping("main.html")
    public String main(){
        return "main";
    }

    @GetMapping("404.html")
    public String notFoud(){
        return "404";
    }

//    @PostMapping("/sys/login")
//    @ResponseBody
//    public BaseResponse login(String username,String password,String captcha) {
//        logger.info("用户名: "+username);
//        logger.info("密码: "+password);
//        logger.info("验证码: "+captcha);
//
//        return new BaseResponse(StatusCode.Success);
//    }

    @PostMapping("/sys/login")
    @ResponseBody
    public BaseResponse login(String username,String password,String captcha){

        logger.info("用户名:{}, 密码:{},验证码:{}",username,password,captcha);

        //获取验证码
        Object kaptcha = ShiroUtil.getSessionAttribute(Constants.KAPTCHA_SESSION_KEY);

        //万能验证码
        if(captcha.equals("1111")){

        }else if(!kaptcha.equals(captcha)){
        //校验验证码
            return new BaseResponse(StatusCode.InvalidCode);

        }


//        if(kaptcha == null){
//            logger.error("验证码已失效！");
//        }
//        Object object=getSessionAttribute(key);
//		if (object==null){
//			throw new CommonException("验证码已失效!");
//		}
//		String newCode=object.toString();
//		getSession().removeAttribute(key);
//		System.out.println("新的验证码："+newCode);
//
//		return newCode;
//        String newCode = kaptcha.toString();
//        getSession().r

        try{
            //提交登录
            Subject subject = SecurityUtils.getSubject();
            //如果未登录，则进行登录
            if(!subject.isAuthenticated()){
                logger.debug("提交登录前");
                UsernamePasswordToken token = new UsernamePasswordToken(username,password);
                subject.login(token);
                logger.debug("提交登录完成");
            }

        }catch (UnknownAccountException e){
            return new BaseResponse(StatusCode.UnknownError);
        }
        catch (IncorrectCredentialsException e){
            return new BaseResponse(StatusCode.AccountPasswordNotMatch);
        }
        catch (LockedAccountException e){
            return new BaseResponse(StatusCode.AccountHasBeenLocked);
        }
        catch (Exception e){
            logger.error("登录异常");
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return new BaseResponse(StatusCode.Success);
    }

//    //生成验证码,有对应到kaptchaConfig文件
//    @GetMapping("captcha.jpg")
//    public void captcha(HttpServletResponse response) throws Exception{
//        byte[] captchaChallengeAsJpeg = null;
//        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
//        try {
//            // 生产验证码字符串并保存到session中
//            String createText = producer.createText();
//            ShiroUtil.getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, createText);
//            // 使用生产的验证码字符串返回一个BufferedImage对象并转为byte写入到byte数组中
//            BufferedImage challenge = producer.createImage(createText);
//            ImageIO.write(challenge, "jpg", jpegOutputStream);
//        } catch (IllegalArgumentException e) {
//            response.sendError(HttpServletResponse.SC_NOT_FOUND);
//            return;
//        }
//        // 定义response输出类型为image/jpeg类型，使用response输出流输出图片的byte数组
//        captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
//        response.setHeader("Cache-Control", "no-store");
//        response.setHeader("Pragma", "no-cache");
//        response.setDateHeader("Expires", 0);
//        response.setContentType("image/jpeg");
//        ServletOutputStream responseOutputStream = response.getOutputStream();
//        responseOutputStream.write(captchaChallengeAsJpeg);
//        responseOutputStream.flush();
//        responseOutputStream.close();
//    }

    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response) throws Exception{
        response.setHeader("Cache-Contrpl","no-cache");
        response.setContentType("image/jpeg");


        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro_session
        ShiroUtil.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY,text);

        ServletOutputStream outputStream = response.getOutputStream();
        ImageIO.write(image,"jpg",outputStream);

        logger.info("验证码为：　"+text);


    }

    //退出登录,重定向到login页面
    @GetMapping("logout")
    public String logout(HttpServletResponse response){
        //销毁当前到shiro到用户session
        ShiroUtil.logout();

        return "redirect:login.html";
    }


}
