package com.mars.springboot.server.controller;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.Constant;
import com.mars.springboot.common.utils.ValidatorUtil;
import com.mars.springboot.model.entity.SysDeptEntity;
import com.mars.springboot.server.service.SysDeptService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//部门管理controller
@RestController
@RequestMapping("/sys/dept")
public class SysDeptController extends AbstractController{

    @Autowired
    private SysDeptService sysDeptService;

    //部门列表~~权限
    @GetMapping("/list")
    public List<SysDeptEntity> list(){

        return sysDeptService.queryAll(Maps.newHashMap());

      //  return sysDeptService.queryList(Maps.newHashMap());  //传一个空的map

    }

    //获取一级部门/顶级部门的deptId
    @GetMapping("/info")
    public BaseResponse info(){

        Map<String,Object> resMap = Maps.newHashMap();
        BaseResponse response = new BaseResponse(StatusCode.Success);
        Long deptId = 0L;
        /*
        $.get(baseURL + "sys/dept/info", function(r){
            //设置根节点code值----可指定根节点，默认为null,"",0,"0"
            table.setRootCodeValue(r.data.deptId);
            */

        try{
            //二级及其以下部门的父部门，不一定是0
            if(getUerId() != Constant.SUPER_ADMIN){

                //涉及到数据视野的问题
                /*List<SysDeptEntity> list = sysDeptService.queryAll(new HashMap<String, Object>());
                Long pId = null;
                for(SysDeptEntity dept : list){
                    if(pId == null){
                        pId = dept.getParentId();
                        continue;
                    }

                    if(pId > dept.getParentId().longValue()){
                        pId = dept.getParentId();
                    }
                }
                deptId = pId;*/
            }

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        resMap.put("deptId",deptId);
        response.setData(resMap);
        return response;
    }

    //获取部门树
    @GetMapping("/select")
    public BaseResponse select(){

        Map<String,Object> resMap = Maps.newHashMap();
        BaseResponse response = new BaseResponse(StatusCode.Success);
        List<SysDeptEntity> deptList = Lists.newLinkedList();
        try{
            deptList = sysDeptService.queryDeptAllList(Maps.newHashMap());
            logger.info("deptList的值是:{}", deptList);
        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        resMap.put("deptList",deptList);
        response.setData(resMap);
        return response;
    }

    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse save(@RequestBody @Validated SysDeptEntity sysDeptEntity, BindingResult result){

        String res = ValidatorUtil.checkResult(result);
        if(StringUtils.isNotBlank(res)){
            return new BaseResponse(StatusCode.InvalidParams.getCode(),res);
        }

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("新增部门，接收的数据是{}",sysDeptEntity);
            sysDeptService.save(sysDeptEntity);
        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }

        return response;
    }

    //根据deptId来详细信息
    @GetMapping("/detail/{deptid}")
    public BaseResponse detail(@PathVariable Long deptid){

        Map<String,Object> resMap = Maps.newHashMap();
        BaseResponse response = new BaseResponse(StatusCode.Success);

        try{
            resMap.put("dept",sysDeptService.getById(deptid));

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }


    //修改部门
    @PostMapping(value = "/update",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse saveOrUpdate(@RequestBody @Validated SysDeptEntity sysDeptEntity, BindingResult result){

        String res = ValidatorUtil.checkResult(result);
        if(StringUtils.isNotBlank(res)){
            return new BaseResponse(StatusCode.Fail.getCode(),res);
        }
        if(sysDeptEntity.getDeptId() == null || sysDeptEntity.getDeptId() <=0 ){
            return new BaseResponse(StatusCode.InvalidParams);

        }

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("更新部门，接收的数据是{}",sysDeptEntity);
            sysDeptService.updateById(sysDeptEntity);
        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }

        return response;
    }


    //删除部门
    @PostMapping(value = "/delete")
    public BaseResponse delete(Long deptId){

        if(deptId == null || deptId <=0 ){
            return new BaseResponse(StatusCode.InvalidParams);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("删除部门，接收的数据是{}",deptId);
            //如果当前节点有子节点，则需要先删除子节点
            List<Long> subIds = sysDeptService.queryDeptIds(deptId);
            if(subIds !=null && !subIds.isEmpty()){
                return new BaseResponse(StatusCode.DeptHasSubDeptCanNotBeDelete);
            }

            sysDeptService.removeById(deptId);

        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }

        return response;
    }

}
