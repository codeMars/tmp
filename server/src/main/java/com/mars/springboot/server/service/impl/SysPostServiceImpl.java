package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.common.utils.QueryUtil;
import com.mars.springboot.model.entity.SysPostEntity;
import com.mars.springboot.model.mapper.SysPostDao;
import com.mars.springboot.server.service.SysPostService;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;

@Service("sysPostService")
public class SysPostServiceImpl extends ServiceImpl<SysPostDao, SysPostEntity> implements SysPostService {

    private static final Logger logger = LoggerFactory.getLogger(SysPostServiceImpl.class);


    //分页模糊查询
    @Override
    public PageUtil queryPage(Map<String, Object> paraMap) {

      //  String searchKey = paraMap.get("search").toString();
        String searchKey = (paraMap.get("search") == null)? "":paraMap.get("search").toString();
        //方法1
//        String currPage = paraMap.get("page").toString();
//        Page<SysPostEntity> queryPage = new Page<>();
//        queryPage.setCurrent(Long.valueOf(currPage));//第几页
//       // queryPage.setSize(); //查询多少数量

        //方法2：调用自封装的分页查询工具
        IPage<SysPostEntity> queryPage = new QueryUtil<SysPostEntity>().getQueryPage(paraMap);

        //sql:SELECT * from sys_post where post_code like '%%' or post_name like '%%'
        QueryWrapper queryWrapper = new QueryWrapper<SysPostEntity>()
                .like(StringUtils.isNotBlank(searchKey),"post_code",searchKey.trim())
                .or(StringUtils.isNotBlank(searchKey))
                .like(StringUtils.isNotBlank(searchKey),"post_name",searchKey.trim());
        IPage<SysPostEntity> resPage =  this.page(queryPage,queryWrapper);
        logger.info("查询结果：{}",resPage);

        return new PageUtil(resPage);
    }


    //新增
    @Override
    public void savePost(SysPostEntity entity) {

        //如果查询的结果不等于null
        if(this.getOne(new QueryWrapper<SysPostEntity>().eq("post_code",entity.getPostCode())) !=null){
            throw new RuntimeException(StatusCode.PostCodeHasExist.getMsg());
        }

        entity.setCreateTime(DateTime.now().toDate());
        save(entity);
    }
    //修改岗位
    @Override
    public void updatePost(SysPostEntity entity) {

        entity.setUpdateTime(DateTime.now().toDate());

        SysPostEntity old = this.getById(entity.getPostId());
        //老的信息不等于空，而且新老postcode不一致，则需要进一步判断
        if(old !=null && !old.getPostCode().equals(entity.getPostCode())){
            //如果postcode存在，则抛异常
            if(this.getOne(new QueryWrapper<SysPostEntity>().eq("post_code",entity.getPostCode())) !=null){
                throw new RuntimeException(StatusCode.PostCodeHasExist.getMsg());

            }

        }

        //如果查询的结果不等于null
        if(this.getOne(new QueryWrapper<SysPostEntity>().eq("post_code",entity.getPostCode())) !=null){
            throw new RuntimeException(StatusCode.PostCodeHasExist.getMsg());
        }

        updateById(entity);
    }


    //删除岗位
    @Override
    public void deletePach(Long[] ids) {
        //第一种写法： mp
  //      removeByIds(Arrays.asList(ids));

        //第二种写法-mybatis
        //原始：ids = [1,2,3,4]   mybaits认可的："1,2,3,4"
        String delIds = Joiner.on(",").join(ids);
        logger.info("删除结果是:{}",delIds);

        baseMapper.deleteBatch(delIds);


    }

}
