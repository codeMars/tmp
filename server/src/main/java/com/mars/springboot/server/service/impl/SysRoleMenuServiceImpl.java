package com.mars.springboot.server.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.mars.springboot.common.utils.CommonUtil;
import com.mars.springboot.model.entity.SysRoleMenuEntity;
import com.mars.springboot.model.mapper.SysRoleMenuDao;
import com.mars.springboot.server.service.SysRoleMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

//角色菜单服务

@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuDao, SysRoleMenuEntity> implements SysRoleMenuService {


    private static final Logger log = LoggerFactory.getLogger(SysRoleMenuServiceImpl.class);


    //维护角色～～菜单关联信息
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(Long roleId, List<Long> menuIdList) {
        //需要新清除旧数据，再插入新数据关联信息
        deleteBatch(Arrays.asList(roleId));

        SysRoleMenuEntity entity;

        if(menuIdList != null && !menuIdList.isEmpty()){

            //遍历menuId
            for (Long mId:menuIdList){
                entity = new SysRoleMenuEntity();
                entity.setMenuId(mId);
                entity.setRoleId(roleId);
                this.save(entity);
            }

        }

    }

  ////  根据角色Id批量删除
    @Override
    public void deleteBatch(List<Long> roleIdList) {

        if(!roleIdList.isEmpty() && roleIdList != null){
            String delIds = Joiner.on(",").join(roleIdList);
            baseMapper.deleteBatch(CommonUtil.concatStrToInt(delIds,","));
        }

    }

    //获取角色对应的菜单列表
    @Override
    public List<Long> queryMenuList(Long roleId) {
        return baseMapper.queryMenuIdList(roleId);
    }
}
