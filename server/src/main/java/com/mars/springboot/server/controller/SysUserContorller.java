package com.mars.springboot.server.controller;


import com.fasterxml.jackson.databind.ser.Serializers;
import com.google.common.collect.Maps;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.Constant;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.common.utils.ValidatorUtil;
import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.server.annotation.LogAnnotation;
import com.mars.springboot.server.service.SysUserPostService;
import com.mars.springboot.server.service.SysUserService;
import com.mars.springboot.server.service.SysUserServiceXml;
import com.mars.springboot.server.util.ShiroUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Map;

@RestController
@RequestMapping("/sys/user")
public class SysUserContorller extends AbstractController {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserServiceXml sysUserServiceXml;

    @GetMapping("/info")
    public BaseResponse currInfo(){

        Map<String,Object> resMap = Maps.newHashMap();
        BaseResponse response = new BaseResponse(StatusCode.Success);
        try {
            //获取当前登录用户到详情

            resMap.put("user",getCurrLoginUser());

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail);

        }
        response.setData(resMap);
        return response;
    }

    @PostMapping("/password")
    public BaseResponse updatePassword(String password,String newPassword){


        //判空
        if(StringUtils.isBlank(password)||StringUtils.isBlank(newPassword)){
            return new BaseResponse(StatusCode.PasswordCanNotBlank);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);

        try {
            //逻辑：校验旧密码是否正确，正确才进行更新。
            SysUserEntity entityUser = getCurrLoginUser();
            final String salt = entityUser.getSalt();
            String oldSaltPassword = ShiroUtil.sha256(password,salt); //查出输入的密码所对应的db密码（带盐）
            final String saltPassword = entityUser.getPassword();

            if(!oldSaltPassword.equals(saltPassword)){
                return new BaseResponse(StatusCode.OldPasswordNotMatch);
            }
            if(password.equals(newPassword)){
                return new BaseResponse(StatusCode.OldNewPasswordIsEqual);
            }

            String newSaltPassword = ShiroUtil.sha256(newPassword,salt);
            //更新密码逻辑
            logger.info("进入更新密码逻辑");

//            //mybatis-plus方式 -- 更新密码
//            if(!sysUserService.updatePassword(entityUser.getUserId(),oldSaltPassword,newSaltPassword)){
//                return new BaseResponse(StatusCode.Fail);
//            }

            //mybatis方式  --- 更新密码
            sysUserServiceXml.updatePassword2(entityUser.getUserId(),oldSaltPassword,newSaltPassword);

        }catch (Exception e){
            response = new BaseResponse(StatusCode.UpdatePasswordFail);
        }
        return response;
    }


    //分页列表模糊查询
    @GetMapping("/list")
  //  @RequiresPermissions(value = {"sys:user:info"})  //因为需要授权，所以要走doGetAuthorizationInfo的逻辑
    public BaseResponse list(@RequestParam Map<String,Object> paraMap){

        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();
        try{
            logger.info("user-lis查询：{}",paraMap);
            PageUtil page = sysUserService.queryPage(paraMap);
            resMap.put("page",page);
        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }

        response.setData(resMap);
        return response;
    }


    //新增用户,输入等userId为null
    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  //  @RequiresPermissions(value = {"sys:user:save"})
    public BaseResponse save(@RequestBody @Validated SysUserEntity userEntity, BindingResult result){
        //字段检查，通用写法
        String res = ValidatorUtil.checkResult(result);
        //空检验
        if(StringUtils.isNotBlank(res)){
            return new BaseResponse(StatusCode.InvalidParams.getCode(),res);
        }
        if(StringUtils.isBlank(userEntity.getPassword())){
            return new BaseResponse(StatusCode.PasswordCanNotBlank);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);

        try{
            sysUserService.saveUser(userEntity);

        }catch (Exception e){
            return  new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //获取用户信息详情
    @GetMapping("/info/{userId}")
    public BaseResponse info(@PathVariable Long userId){
        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();
        try{
            logger.info("查询用户信息:{}",userId);
            resMap.put("user",sysUserService.getInfo(userId));

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }


    //修改用户
    @PostMapping(value = "/update",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
 //   @RequiresPermissions(value = {"sys:user:update"})
    public BaseResponse update(@RequestBody SysUserEntity userEntity){

//        if(StringUtils.isBlank(userEntity.getPassword())){
//            return new BaseResponse(StatusCode.PasswordCanNotBlank);
//        }
        BaseResponse response = new BaseResponse(StatusCode.Success);

        try{
            logger.info("修改用户信息:{}",userEntity);
            sysUserService.updateUser(userEntity);

        }catch (Exception e){
            return  new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }


    //删除用户
    @PostMapping(value = "/delete",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  //  @RequiresPermissions(value = {"sys:user:delete"})
    public BaseResponse delete(@RequestBody Long[] userIds){

        if(userIds == null || userIds.length <=0){
            return new BaseResponse(StatusCode.InvalidParams);
        }

        //超级管理员和当前登录用户不能删除
        //写法1：
        if(Arrays.asList(userIds).contains(Constant.SUPER_ADMIN)){
        //写法2：
       // if(ArrayUtils.contains(userIds,Constant.SUPER_ADMIN))
             return new BaseResponse(StatusCode.SysUserCanNotBeDelete);
        }
        if(ArrayUtils.contains(userIds,getCurrLoginUserId())){
            return new BaseResponse(StatusCode.CurrUserCanNotBeDelete);
        }

        BaseResponse response = new BaseResponse(StatusCode.Success);

        try{
            logger.info("删除用户信息:{}",userIds);
            sysUserService.deleteUser(userIds);
        }catch (Exception e){
            return  new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }


    //重置密码
    @PostMapping(value = "/psd/reset",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
 //   @RequiresPermissions(value = {"sys:user:resetPsd"})
    @LogAnnotation("重置密码")
    public BaseResponse resetPsd(@RequestBody Long[] userIds){

        if(userIds == null || userIds.length <=0){
            return new BaseResponse(StatusCode.InvalidParams);
        }

        //超级管理员和当前登录用户不能重置
        //写法1：
        if(Arrays.asList(userIds).contains(Constant.SUPER_ADMIN)){
            return new BaseResponse(StatusCode.SysUserAndCurrUserCanNotResetPsd);
        }
        if(ArrayUtils.contains(userIds,getCurrLoginUserId())){
            return new BaseResponse(StatusCode.SysUserAndCurrUserCanNotResetPsd);
        }

        BaseResponse response = new BaseResponse(StatusCode.Success);

        try{
            logger.info("重置用户信息:{}",userIds);
            sysUserService.resetPsd(userIds);
        }catch (Exception e){
            return  new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

}
