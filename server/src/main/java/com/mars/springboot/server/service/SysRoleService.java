package com.mars.springboot.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.model.entity.SysRoleDeptEntity;
import com.mars.springboot.model.entity.SysRoleEntity;

import java.util.Map;


public interface SysRoleService extends IService<SysRoleEntity> {


    public PageUtil queryPage(Map<String,Object> paraMap);

    void saveRole(SysRoleEntity entity);

    void updateOrUpdate(SysRoleEntity entity);


    void deletebBathch(Long[] ids);

}
