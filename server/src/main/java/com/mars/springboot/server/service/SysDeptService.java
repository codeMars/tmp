package com.mars.springboot.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.model.entity.SysDeptEntity;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SysDeptService extends IService<SysDeptEntity> {

    List<SysDeptEntity> queryDeptAllList(Map<String,Object> map);

    List<SysDeptEntity> queryList(Map<String,Object> map);

    //查询当前节点到子节点
    List<Long> queryDeptIds(Long parentId);

    List<SysDeptEntity> queryAll(Map<String,Object> map);

    List<Long> getSubDeptIdsList(Long id);


}


