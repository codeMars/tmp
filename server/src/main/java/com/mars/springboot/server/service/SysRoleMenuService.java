package com.mars.springboot.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.model.entity.SysRoleDeptEntity;
import com.mars.springboot.model.entity.SysRoleMenuEntity;

import java.util.List;

public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {


    void saveOrUpdate(Long roleId, List<Long> menuIdList);

    void deleteBatch(List<Long> roleIdList);

    List<Long> queryMenuList(Long roleId);

}
