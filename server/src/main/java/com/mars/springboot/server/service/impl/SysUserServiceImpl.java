package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.Constant;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.common.utils.QueryUtil;
import com.mars.springboot.model.entity.SysDeptEntity;
import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.model.entity.SysUserPostEntity;
import com.mars.springboot.model.entity.SysUserRoleEntity;
import com.mars.springboot.model.mapper.SysUserDao;
import com.mars.springboot.server.service.SysDeptService;
import com.mars.springboot.server.service.SysUserPostService;
import com.mars.springboot.server.service.SysUserRoleService;
import com.mars.springboot.server.service.SysUserService;
import com.mars.springboot.server.util.ShiroUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao,SysUserEntity> implements SysUserService {


    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private SysUserPostService sysUserPostService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    //更新密码 - 借助于mybatis-plus方式，  如使用mybatis方式，则使用Dao层
    @Override
    public boolean updatePassword(Long userId, String oldPassword, String newPassword) {
        SysUserEntity entity = new SysUserEntity();
        entity.setPassword(newPassword);
        return this.update(entity,new QueryWrapper<SysUserEntity>().eq("user_id",userId).eq("password",oldPassword));

    }


    //分页列表模糊查询
    @Override
    public PageUtil queryPage(Map<String, Object> paraMap) {
        String search = (paraMap.get("username")) !=null ? (String) paraMap.get("username"):"";

        //定义了一个SysUser的ipage
        IPage<SysUserEntity> ipage = new QueryUtil<SysUserEntity>().getQueryPage(paraMap);

        //查询包装器, 可以通过usrname 或 name来查询
        QueryWrapper wrapper = new QueryWrapper<SysUserEntity>().like(StringUtils.isNotBlank(search),"username",search.trim())
                .or(StringUtils.isNotBlank(search.trim()))
                .like(StringUtils.isNotBlank(search),"name",search.trim());

        IPage<SysUserEntity> resPage = this.page(ipage,wrapper);
        SysDeptEntity deptEntity;
        //因上述结果比页面显示缺少两个字段，故需定义组装
        //resPage.getRecords() 获取对象的信息
        //获取用户所属部门和岗位信息
        for(SysUserEntity userEntity : resPage.getRecords()){
            deptEntity = sysDeptService.getById(userEntity.getDeptId());
            if(deptEntity != null && StringUtils.isNotBlank(deptEntity.getName())){
                userEntity.setDeptName(deptEntity.getName());
            }else {
                userEntity.setDeptName("");
            }
            String postName = sysUserPostService.getPostNameByUserId(userEntity.getUserId());
            userEntity.setPostName(postName);
        }
        return new PageUtil(resPage);
    }

    //保存用户
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUser(SysUserEntity userEntity) {

        //查询用户信息是否已存在
         if(this.getOne(new QueryWrapper<SysUserEntity>().eq("username",userEntity.getUsername())) != null ){
             throw new RuntimeException("用户名已存在");
         }

        //创建时间
        userEntity.setCreateTime(new Date());

        //加密密码串
        String salt = RandomStringUtils.randomAlphanumeric(10); //创建盐
        String password = ShiroUtil.sha256(userEntity.getPassword(),salt); //加盐生成的密码串
        userEntity.setPassword(password);
        userEntity.setSalt(salt);

        this.save(userEntity);

        //维护好 用户～角色的关联关系
        sysUserRoleService.saveOrUpdate(userEntity.getUserId(),userEntity.getRoleIdList());

        //维护好 用户～岗位的关联关系
        sysUserPostService.saveOrUpdate(userEntity.getUserId(),userEntity.getPostIdList());

    }


    //获取用户详情，包括其分配的岗位，角色等信息
    @Override
    public SysUserEntity getInfo(Long userId) {
        //获取userEntity的信息
        SysUserEntity entity = this.getById(userId);

        //获取用户～角色的关联信息
        List<Long> userRoleIdList = sysUserRoleService.queryRoleIdList(userId);
        entity.setRoleIdList(userRoleIdList);

        //获取用户～岗位的关联信息
        List<Long> userPostIdList = sysUserPostService.queryPostIdList(userId);
        entity.setPostIdList(userPostIdList);
        return entity;
    }

    //更新用户信息
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUser(SysUserEntity userEntity) {

        //获取数据库原有的用户信息
        SysUserEntity oldEntity = this.getById(userEntity.getUserId());
        if(oldEntity == null){
            return;
        }

        if(!oldEntity.getUsername().equals(userEntity.getUsername())){
            //查询用户信息是否已存在
            if(this.getOne(new QueryWrapper<SysUserEntity>().eq("username",userEntity.getUsername())) != null ){
                throw new RuntimeException("修改后的用户名已存在");
            }
        }

        //如果修改时填写的密码不为空，则。。修改密码
        if(StringUtils.isNotBlank(userEntity.getPassword())){
            //加密密码串
            String newPassword = ShiroUtil.sha256(userEntity.getPassword(),oldEntity.getSalt()); //加盐生成的密码串
            userEntity.setPassword(newPassword);
        }

        this.updateById(userEntity);

        //维护好 用户～角色的关联关系
        sysUserRoleService.saveOrUpdate(userEntity.getUserId(),userEntity.getRoleIdList());

        //维护好 用户～岗位的关联关系
        sysUserPostService.saveOrUpdate(userEntity.getUserId(),userEntity.getPostIdList());

    }

    //删除用户，除了删除用户本身信息，还需要删除用户～角色，用户～岗位关联信息
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteUser(Long[] userIds) {
        List<Long> userList = Arrays.asList(userIds);
        if(userIds != null && userIds.length>0){
            //删除用户信息
            this.removeByIds(userList);

            for(Long uId : userList){
                //删除用户～角色关联信息
                sysUserRoleService.remove(new QueryWrapper<SysUserRoleEntity>().eq("user_id",uId));
                //删除用户～岗位关联信息
                sysUserPostService.remove(new QueryWrapper<SysUserPostEntity>().eq("user_id",uId));
            }
        }
    }

    //重置用户密码
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void resetPsd(Long[] userIds) {
        List<Long> userList = Arrays.asList(userIds);
        if(userIds != null && userIds.length>0){
            for(Long uId : userList){
            //获取用户信息
            SysUserEntity entity = this.getById(uId);
            String newPass = ShiroUtil.sha256(Constant.DefaultPassword,entity.getSalt());
            entity.setPassword(newPass);
            this.updateById(entity);
            }
        }
    }
}
