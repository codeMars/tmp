package com.mars.springboot.server.annotation;


import net.bytebuddy.implementation.bind.annotation.RuntimeType;

import java.lang.annotation.*;

@Target(ElementType.METHOD)  //  目标是方法
@Retention(RetentionPolicy.RUNTIME)  //运行时执行
@Documented
public @interface LogAnnotation {

    String value() default "";


}
