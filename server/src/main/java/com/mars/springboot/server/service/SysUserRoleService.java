package com.mars.springboot.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.model.entity.SysRoleMenuEntity;
import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.model.entity.SysUserRoleEntity;

import java.util.List;

public interface SysUserRoleService extends IService<SysUserRoleEntity> {


    void deleteBatch(List<Long> roleIdList);

    void saveOrUpdate(Long userId, List<Long> roleIdList);

    List<Long> queryRoleIdList(Long userId);


}
