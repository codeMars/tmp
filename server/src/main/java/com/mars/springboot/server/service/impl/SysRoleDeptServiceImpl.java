package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.mars.springboot.common.utils.CommonUtil;
import com.mars.springboot.model.entity.SysRoleDeptEntity;
import com.mars.springboot.model.mapper.SysRoleDeptDao;
import com.mars.springboot.server.service.SysRoleDeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;


@Service("sysRoleDeptService")
public class SysRoleDeptServiceImpl  extends ServiceImpl<SysRoleDeptDao,SysRoleDeptEntity>
        implements SysRoleDeptService {


    private static final Logger log = LoggerFactory.getLogger(SysRoleDeptServiceImpl.class);


    //维护角色～～部门信息
    @Override
    @Transactional(rollbackFor = Exception.class)  //事物回滚
    public void saveOrUpdate(Long roleId, List<Long> deptIdList) {

        //需要新清除旧数据，再插入新数据关联信息
        deleteBatch(Arrays.asList(roleId));


        SysRoleDeptEntity entity;

        if(deptIdList != null && !deptIdList.isEmpty()){

            //遍历menuId
            for (Long deptId:deptIdList){
                entity = new SysRoleDeptEntity();
                entity.setDeptId(deptId);
                entity.setRoleId(roleId);
                this.save(entity);
            }

        }

    }

    //根据角色Id批量删除
    @Override
    public void deleteBatch(List<Long> deptIdList) {

        if(!deptIdList.isEmpty() && deptIdList != null){
            String delIds = Joiner.on(",").join(deptIdList);
            baseMapper.deleteBatch(CommonUtil.concatStrToChar(delIds,","));
        }
    }

    //根据角色ID获取部门ID列表
    @Override
    public List<Long> queryDeptIdList(Long roleId) {
        return baseMapper.queryDeptIdListByRoleId(roleId);
    }
}
