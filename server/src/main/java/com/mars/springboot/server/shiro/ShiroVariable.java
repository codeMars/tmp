package com.mars.springboot.server.shiro;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;

//推送给前端使用的shiro对象变量
@Component
public class ShiroVariable {

     //判断当前登录的用户是否有指定的权限
    public Boolean hasPermission(String permission){
        Subject subject = SecurityUtils.getSubject(); //获取主体
        if( subject != null && subject.isPermitted(permission)){
            return true;
        }
        return false;
    }

}
