package com.mars.springboot.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WEB应用访问静态资源通用配置
 * Created by Administrator on 2019/10/27.
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        //前端访问statics目录的时候，映射到本地的静态资源路径
        registry.addResourceHandler("/statics/**").addResourceLocations("classpath:/statics/");
    }

}
