package com.mars.springboot.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by Administrator on 2019/10/27.
 */

@Controller
public class SysPageController {


    @GetMapping("/modules/{module}/{page}.html")
    public String page(@PathVariable String module,@PathVariable String page){

        return "modules/"+module+"/"+page;
    }




}
