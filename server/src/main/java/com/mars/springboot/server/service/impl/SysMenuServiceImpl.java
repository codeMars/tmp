package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.mars.springboot.common.utils.Constant;
import com.mars.springboot.model.entity.SysMenuEntity;
import com.mars.springboot.model.entity.SysRoleMenuEntity;
import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.model.mapper.SysMenuDao;
import com.mars.springboot.model.mapper.SysUserDao;
import com.mars.springboot.server.service.SysMenuService;
import com.mars.springboot.server.service.SysRoleMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenuEntity> implements SysMenuService {

    private static final Logger log= LoggerFactory.getLogger(SysMenuServiceImpl.class);

    @Autowired
    SysRoleMenuService sysRoleMenuService;

    @Autowired
    SysUserDao sysUserDao;


    //获取所有菜单列表
    @Override
    public List<SysMenuEntity> queryAll(){

        return baseMapper.queryList();

    }

    @Override
    public List<SysMenuEntity> queryNotButtonList() {

        return baseMapper.queryNotButtonList();
    }

    @Override
    public List<SysMenuEntity> queryByParentId(Long menuId) {
        return baseMapper.queryListParentId(menuId);
    }


    //删除
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long menuId) {
        removeById(menuId);
        sysRoleMenuService.remove(new QueryWrapper<SysRoleMenuEntity>().eq("menu_id",menuId));
    }


    //获取首页左边导航菜单栏~~~~菜单的重要入口
    @Override
    public List<SysMenuEntity> getUserMenuList(Long currUserId) {
        List<SysMenuEntity> list = Lists.newLinkedList();
        if(currUserId == Constant.SUPER_ADMIN){
            list = this.getAllMenuList(null);
            log.info("list: {}",list);
        }else {
            //非超级管理员情况，根据分配给用户的角色～菜单的关联信息来获取
            List<Long> menuIdList = sysUserDao.queryAllMenuId(currUserId);
            list = this.getAllMenuList(menuIdList);
        }
        return list;
    }

    //获取所有菜单～需要全部遍历～递归~~ 菜单递归重要入口
    private List<SysMenuEntity> getAllMenuList(List<Long> menuIdList){
        //找出一级菜单
        List<SysMenuEntity> menuList = this.queryListByParentId(0L,menuIdList);

//        //递归获取一级菜单下的子菜单
        getMenuTrees(menuList,menuIdList);

        return menuList;
    }

    //根据父菜单id查询子菜单列表～~找出一级菜单列表
    //menuIdList: 针对非超级管理员的处理，获取分配角色下的菜单id列表
    private List<SysMenuEntity> queryListByParentId(Long parentId,List<Long> menuIdList){

        List<SysMenuEntity> menuList = baseMapper.queryListParentId(parentId);
        if(menuIdList.isEmpty() || menuIdList == null){
            return menuList;
        }
        //在所有的一级菜单中 找出存在于："用户分配的菜单列表中"
        List<SysMenuEntity> userMenuList = Lists.newLinkedList();
        for(SysMenuEntity entity:menuList){
            if(menuIdList.contains(entity.getMenuId())){
                userMenuList.add(entity);
            }
        }
        return userMenuList;
    }

    private List<SysMenuEntity> getMenuTrees(List<SysMenuEntity> menuList,List<Long> menuIdList){
        List<SysMenuEntity> subMenuList = Lists.newLinkedList();
        List<SysMenuEntity> tempList;

        for(SysMenuEntity entity: menuList){
//            //当前菜单的类型为菜单，则开始进行遍历，递归终止：不是目录
//            if(entity.getType() == Constant.MenuType.CATALOG.getValue()){
//                tempList = this.queryListByParentId(entity.getMenuId(),menuIdList);
//                entity.setList(getMenuTrees(tempList,menuIdList));
//            }
            tempList = this.queryListByParentId(entity.getMenuId(),menuIdList);
            if(tempList != null && !tempList.isEmpty()){
                entity.setList(getMenuTrees(tempList,menuIdList));
            }
            subMenuList.add(entity);

        }

        return subMenuList;
    }


}


//        import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//        import com.debug.pmp.common.utils.CommonUtil;
//        import com.debug.pmp.model.entity.SysRoleMenuEntity;
//        import com.debug.pmp.model.mapper.SysRoleMenuDao;
//        import com.debug.pmp.server.service.SysRoleMenuService;
//        import com.google.common.base.Joiner;
//        import org.slf4j.Logger;
//        import org.slf4j.LoggerFactory;
//        import org.springframework.stereotype.Service;
//        import org.springframework.transaction.annotation.Transactional;
//
//        import java.util.Arrays;
//        import java.util.List;
//
//
//@Service("sysRoleMenuService")
//public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuDao,SysRoleMenuEntity> implements SysRoleMenuService{
//
//    private static final Logger log= LoggerFactory.getLogger(SysRoleMenuServiceImpl.class);
//
//    //维护角色~菜单关联信息
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void saveOrUpdate(Long roleId, List<Long> menuIdList) {
//        //需要先清除旧的关联数据，再插入新的关联信息
//        deleteBatch(Arrays.asList(roleId));
//
//        SysRoleMenuEntity entity;
//        if (menuIdList!=null && !menuIdList.isEmpty()){
//            for (Long mId:menuIdList){
//                entity=new SysRoleMenuEntity();
//                entity.setRoleId(roleId);
//                entity.setMenuId(mId);
//                this.save(entity);
//            }
//        }
//
//    }
//
//
//    //根据角色id批量删除
//    @Override
//    public void deleteBatch(List<Long> roleIds) {
//        if (roleIds!=null && !roleIds.isEmpty()){
//            String delIds= Joiner.on(",").join(roleIds);
//            baseMapper.deleteBatch(CommonUtil.concatStrToInt(delIds,","));
//        }
//    }
//
//    //获取角色对应的菜单列表
//    @Override
//    public List<Long> queryMenuIdList(Long roleId) {
//        return baseMapper.queryMenuIdList(roleId);
//    }
//
//}
























