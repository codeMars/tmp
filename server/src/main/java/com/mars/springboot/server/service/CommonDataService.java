package com.mars.springboot.server.service;


import java.util.Set;

public interface CommonDataService {

    //获取当前登录用户所在部门数据id列表
    Set<Long> getCurrUserDataDeptIds();

    //将 部门数据id列表 转化为 id拼接的字符串
    String getCurrUserDataDeptIdsStr();


}
