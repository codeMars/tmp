package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.mars.springboot.model.entity.SysUserPostEntity;
import com.mars.springboot.model.mapper.SysUserPostDao;
import com.mars.springboot.server.service.SysUserPostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service("sysUserPostService")
public class SysUserPostServiceImpl  extends ServiceImpl<SysUserPostDao,SysUserPostEntity> implements SysUserPostService {

    private static final Logger log = LoggerFactory.getLogger(SysUserPostServiceImpl.class);


    //根据用户id获取岗位，如果有多个，则采用，号拼接
    @Override
    public String getPostNameByUserId(Long userId) {
/*
        //方法1：
        //返回的是一个set
        Set<String> userSet=baseMapper.getPostNamesByUserId(userId);
        //set转String
        if(userSet != null && !userSet.isEmpty()){
            return Joiner.on(",").join(userSet);
        }else {
            return "";
        }
        */

        //方法2：
/*        StringBuilder sb = new StringBuilder("");
        List<SysUserPostEntity> userList = baseMapper.getByUserId(userId);
        if(userList !=null && !userList.isEmpty()){
            for(SysUserPostEntity entity: userList){
                sb.append(entity.getPostName()).append(",");
            }
        }
        //处理最后一个，号
        String result = sb.toString();
        if(result.lastIndexOf(",")>= 0){
            result = result.substring(0,result.lastIndexOf(","));
        }*/

        //方法3： java8 写法
        String result ="";
        List<SysUserPostEntity> userList = baseMapper.getByUserId(userId);
        if(userList !=null && !userList.isEmpty()) {
            //把getname的对象转化成
           Set<String> userSet = userList.stream().map(SysUserPostEntity::getPostName).collect(Collectors.toSet());
           result = Joiner.on(",").join(userSet);
        }

        return result;
    }


    //维护好～用户与岗位的关联关系
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(Long userId, List<Long> postIdList) {

        //删除原有的关联关系
        this.remove(new QueryWrapper<SysUserPostEntity>().eq("user_id",userId));

        if(postIdList != null && !postIdList.isEmpty()){
            SysUserPostEntity userPostEntity;
            for(Long pid:postIdList){
                userPostEntity = new SysUserPostEntity();
                userPostEntity.setPostId(pid);
                userPostEntity.setUserId(userId);
                this.save(userPostEntity);
            }
        }
    }

    //获取用户的部门信息
    @Override
    public List<Long> queryPostIdList(Long userId) {
        return baseMapper.queryPostIdList(userId);
    }
}
