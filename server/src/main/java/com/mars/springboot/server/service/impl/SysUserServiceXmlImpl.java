package com.mars.springboot.server.service.impl;

import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.model.mapper.SysUserDao;
import com.mars.springboot.server.service.SysUserServiceXml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("sysUserServiceXml")
public class SysUserServiceXmlImpl implements SysUserServiceXml {

    @Autowired
    private SysUserDao sysUserDao;

    //更新密码 -  使用mybatis方式
    @Override
    public void updatePassword2(Long userId, String oldPassword, String newPassword) {

         sysUserDao.updatePassword2(userId,oldPassword,newPassword);

    }


}
