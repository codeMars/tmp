package com.mars.springboot.server.controller;


import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2019/10/26.
 */

@Controller
//@RequestMapping("/base")
public class BaseController {

    private static final Logger log = LoggerFactory.getLogger(BaseController.class);

    /***
     *  第一个案例: 返回json的交互
     */
//    @RequestMapping(value = "/info",method = RequestMethod.GET)
    @ResponseBody //不加，则返回html页面
    @GetMapping("/info")
    public BaseResponse info(String name){
        BaseResponse response = new BaseResponse(StatusCode.Success);
        if(StringUtils.isBlank(name)){
            name = "TMP管理平台";
        }

        response.setData(name);

        return response;

    }

    /***
     * 第二个案例: 页面跳转，返回页面的交互
     */
    @GetMapping("/page")
    public String pageOne(String name, ModelMap modelMap){
        BaseResponse response = new BaseResponse(StatusCode.Success);
        if(StringUtils.isBlank(name)){
            name = "TMP管理平台";
        }
        modelMap.put("name",name);
        modelMap.put("app","App值");

        return "pageOne";

    }

    @GetMapping("/login")
    public String baseLogin(){
        return "login";
    }

}
