package com.mars.springboot.server.controller;

import com.mars.springboot.model.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

/**
 * Created by Administrator on 2019/10/27.
 */

@Controller
public abstract class AbstractController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    //获取当前登录用户详情
    protected SysUserEntity getCurrLoginUser(){
        //获取当前登录用户到详情
        SysUserEntity userEntity = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        return userEntity;
    }

    //获取当前登录用户ID
    protected Long getCurrLoginUserId(){
        return getCurrLoginUser().getUserId();
    }

    //获取当前登录用户ID
    protected Long getUerId(){
        return getCurrLoginUser().getUserId();
    }


    //获取当前登录用户姓名
    protected String getCurrLoginUserName(){
        return getCurrLoginUser().getName();
    }
    //获取当前登录用户部门号
    protected Long getCurrLoginUserDeptId(){
        return getCurrLoginUser().getDeptId();
    }
}
