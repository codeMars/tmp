package com.mars.springboot.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.model.entity.SysLogEntity;

import java.util.Map;

public interface SysLogService extends IService<SysLogEntity> {

    PageUtil queryPage(Map<String,Object> map);

    void truncate();

}



