package com.mars.springboot.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.model.entity.SysMenuEntity;
import com.mars.springboot.model.entity.SysUserEntity;

import java.util.List;


public interface SysMenuService extends IService<SysMenuEntity> {


//    List<SysMenuEntity> queryList();


    List<SysMenuEntity> queryNotButtonList();

    List<SysMenuEntity> queryByParentId(Long menuId);

    void delete(Long menuId);

//    List<SysMenuEntity> getUserMenuList(Long currUserId);

    List<SysMenuEntity> queryAll();

    List<SysMenuEntity> getUserMenuList(Long currUserId);


}
