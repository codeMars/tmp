package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.common.utils.QueryUtil;
import com.mars.springboot.model.entity.SysRoleEntity;
import com.mars.springboot.model.mapper.SysRoleDao;
import com.mars.springboot.server.service.SysRoleDeptService;
import com.mars.springboot.server.service.SysRoleMenuService;
import com.mars.springboot.server.service.SysRoleService;
import com.mars.springboot.server.service.SysUserRoleService;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Service("sysRoleService")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao,SysRoleEntity> implements SysRoleService {


    private static final Logger log = LoggerFactory.getLogger(SysRoleServiceImpl.class);


    @Autowired
    private SysRoleDeptService sysRoleDeptService;

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    //分页列表模糊查询
    @Override
    public PageUtil queryPage(Map<String, Object> paraMap) {

        String search = paraMap.get("search") != null ? paraMap.get("search").toString():"";

        //页面， 如总的页面信息
        IPage<SysRoleEntity> iPage = new QueryUtil<SysRoleEntity>().getQueryPage(paraMap);
        log.info("iPage:{}",iPage);
        //根据rolename查询，模糊查询， 查询条件
        QueryWrapper wrapper = new QueryWrapper<SysRoleEntity>()
                .like("role_name",search);

        IPage<SysRoleEntity> resPage = this.page(iPage,wrapper);
        log.info("resPage:{}",resPage);

        return new PageUtil(resPage);
    }

    //新增
    @Override
    public void saveRole(SysRoleEntity role) {
        role.setCreateTime(DateTime.now().toDate());
        this.save(role);

        //插入角色菜单关联信息
        sysRoleMenuService.saveOrUpdate(role.getRoleId(),role.getMenuIdList());
        //插入角色部门关联信息
        sysRoleDeptService.saveOrUpdate(role.getRoleId(),role.getDeptIdList());
    }

    //更新
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateOrUpdate(SysRoleEntity role) {

        this.updateById(role);

        //更新-插入角色菜单关联信息
        sysRoleMenuService.saveOrUpdate(role.getRoleId(),role.getMenuIdList());
        //更新-插入角色部门关联信息
        sysRoleDeptService.saveOrUpdate(role.getRoleId(),role.getDeptIdList());

    }

    //批量删除
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deletebBathch(Long[] ids) {

        List<Long> idList = Arrays.asList(ids);

        //删除角色
        this.removeByIds(idList);

        //删除角色对应的菜单关联信息
        sysRoleMenuService.deleteBatch(idList);

        //删除角色对应的部门关联信息
        sysRoleDeptService.deleteBatch(idList);

        //删除角色对应的用户关联信息
        sysUserRoleService.deleteBatch(idList);

    }
}