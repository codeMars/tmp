package com.mars.springboot.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.mars.springboot.common.utils.Constant;
import com.mars.springboot.model.entity.SysDeptEntity;
import com.mars.springboot.model.mapper.SysDeptDao;
import com.mars.springboot.server.service.CommonDataService;
import com.mars.springboot.server.service.SysDeptService;
import com.mars.springboot.server.util.ShiroUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service("sysDeptService")
public class SysDeptServiceImpl extends ServiceImpl<SysDeptDao, SysDeptEntity> implements SysDeptService {

    private static final Logger logger = LoggerFactory.getLogger(SysDeptServiceImpl.class);

    @Autowired
    private CommonDataService commonDataService;

    //查询所有部门
    @Override
    public List<SysDeptEntity> queryDeptAllList(Map<String, Object> map) {
        return baseMapper.queryDeptAllList(map);
    }

    //查询所有部门
    @Override
    public List<SysDeptEntity> queryList(Map<String, Object> map) {
//        baseMapper.queryDeptAll();

        return baseMapper.queryList(map);
    }


    //查询所有部门列表～～涉及到部门数据权限的控制
    @Override
    public List<SysDeptEntity> queryAll(Map<String, Object> map) {
//        baseMapper.queryDeptAll();

        logger.info("1~~~~{}:2~~~~{}",ShiroUtil.getUserId(),Constant.SUPER_ADMIN);
        if(ShiroUtil.getUserId() != Constant.SUPER_ADMIN){
            String depString = commonDataService.getCurrUserDataDeptIdsStr();
            map.put("deptDataIds",depString );
            logger.info("map~~deptDataIds:{}",depString);
            return baseMapper.queryListV2(map);
        }
            return baseMapper.queryListV1();
    }

    //根据父部门id查询子部门的ids
    @Override
    public List<Long> queryDeptIds(Long parentId) {

        return baseMapper.queryDeptIds(parentId);
    }

    //获取当前部门的所有子部门～～递归方法
    @Override
    public List<Long> getSubDeptIdsList(Long deptId) {
        List<Long> deptIdList = Lists.newLinkedList();

        //第一级的list
        List<Long> subDeptIdsList = baseMapper.queryDeptIds(deptId);
        this.getDeptTreeList(subDeptIdsList,deptIdList);

        return deptIdList;
    }

    /**
     * 递归方法
     * @param subDeptIdsList  第一级的deptId
     *  @param deptIdList  每次递归时循环存储的结果数据id列表
     */
    public void getDeptTreeList(List<Long> subDeptIdsList,List<Long> deptIdList){
        List<Long> list;

        for(Long subId:subDeptIdsList){
            list = baseMapper.queryDeptIds(subId);

            if(list != null && !list.isEmpty()){
                //执行递归调用
                this.getDeptTreeList(list,deptIdList);
            }
            deptIdList.add(subId);
        }
    }

}
