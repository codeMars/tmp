package com.mars.springboot.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.model.entity.SysUserPostEntity;

import java.util.List;

public interface SysUserPostService extends IService<SysUserPostEntity> {


    String getPostNameByUserId(Long userId);

    void saveOrUpdate(Long userId, List<Long> postIdList);

    List<Long> queryPostIdList(Long userId);


}
