package com.mars.springboot.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.model.entity.SysRoleDeptEntity;

import java.util.List;


public interface SysRoleDeptService extends IService<SysRoleDeptEntity> {


    void saveOrUpdate(Long roleId, List<Long> deptIdList);

    void deleteBatch(List<Long> deptIdList);

//    //根据父级部门id查询子部门id列表
//    List<Long> queryDeptIds(List<Long> parentId);

    //获取角色的部门列表
    List<Long> queryDeptIdList(Long roleId);



}
