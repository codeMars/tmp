package com.mars.springboot.server.controller;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.Constant;
import com.mars.springboot.common.utils.ValidatorUtil;
import com.mars.springboot.model.entity.SysDeptEntity;
import com.mars.springboot.model.entity.SysMenuEntity;
import com.mars.springboot.model.entity.SysUserEntity;
import com.mars.springboot.server.service.SysMenuService;
import org.apache.commons.collections4.Get;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController{



    @Autowired
    private SysMenuService sysMenuService;


    @GetMapping("/list")
    public List<SysMenuEntity> list() {

//        //第一种方法：借助mp
//        List<SysMenuEntity> list = sysMenuService.list();
//        if (list != null && !list.isEmpty()) {
//            //for-each
//            list.stream().forEach(entity -> {
//             //实现方法  //根据ParentID去查，返回一个Menu的实体类
//                SysMenuEntity menu = sysMenuService.getById(entity.getParentId());
//                entity.setParentName((menu != null && StringUtils.isNotBlank(menu.getName())) ? menu.getName() : "");
//            }
//            );
//        }
//        return list;

        //第二种方法：自己写sql
        return sysMenuService.queryAll();

    }

    //获取树形层级列表数据
    @GetMapping("/select")
    public BaseResponse select(){

        Map<String,Object> resMap = Maps.newHashMap();
        BaseResponse response = new BaseResponse(StatusCode.Success);

        try{
            List<SysMenuEntity> list = sysMenuService.queryNotButtonList();
            SysMenuEntity root = new SysMenuEntity();
            root.setMenuId(Constant.TOP_MENU_ID); //顶级菜单
            root.setName(Constant.TOP_MENU_NAME);
            root.setParentId(-1L);
            root.setOpen(true); //设置ztree默认菜单是打开(true)还是关闭(false)
            list.add(root);

            resMap.put("menuList",list);

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }

    //新增菜单
    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse save(@RequestBody SysMenuEntity entity){

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("新增菜单～接收到数据：{}",entity);
            //数据校验
            String result = this.validateForm(entity);
            if(StringUtils.isNotBlank(result)){
                return new BaseResponse(StatusCode.Fail.getCode(),result);
            }
            sysMenuService.save(entity);

        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }


        return response;
    }


    //获取菜单详情
    @GetMapping("/info/{menuId}")
    public BaseResponse info(@PathVariable Long menuId){

        if(menuId == null || menuId <=0){
            return new BaseResponse(StatusCode.InvalidParams);
        }

        Map<String,Object> resMap = Maps.newHashMap();
        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            resMap.put("menu",sysMenuService.getById(menuId));

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }


    //修改菜单
    @PostMapping(value = "/update",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse update(@RequestBody SysMenuEntity entity){

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("修改菜单～接收到数据：{}",entity);
            //数据校验
            String result = this.validateForm(entity);
            if(StringUtils.isNotBlank(result)){
                return new BaseResponse(StatusCode.Fail.getCode(),result);
            }
            sysMenuService.updateById(entity);

        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }


        return response;
    }

    //删除菜单
    @PostMapping(value = "/delete")
    public BaseResponse delete(Long menuId){

        if(menuId == null || menuId<=0){
            return new BaseResponse(StatusCode.InvalidParams);
        }

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("删除菜单～接收到数据id：{}",menuId);
            SysMenuEntity entity = sysMenuService.getById(menuId); //获取当前id的信息
            if(entity == null){
                return new BaseResponse(StatusCode.InvalidParams);
            }
            List<SysMenuEntity> entityList = sysMenuService.queryByParentId(entity.getMenuId());

            if(entityList !=null || !entityList.isEmpty()){
                return new BaseResponse(StatusCode.MenuHasSubMenuListCanNotDelete);
            }

            sysMenuService.delete(menuId);
        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }


        return response;
    }


    //获取树形层级列表数据
    @GetMapping("/nav")
    public BaseResponse nav(){
        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();
        try{
            logger.info("当前用户为： {}",this.getCurrLoginUser());
            List<SysMenuEntity> entities = sysMenuService.getUserMenuList(getUerId());

            resMap.put("menuList",entities);

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }




    private String validateForm(SysMenuEntity menu){

        if(!StringUtils.isNotBlank(menu.getName())){
            return "菜单名称不能为空";
        }

        if(menu.getParentName() == null){
            return "上级菜单不能为空！";
        }

        if(menu.getType() == Constant.MenuType.MENU.getValue()){
            if(!StringUtils.isNotBlank(menu.getUrl())){
                return "菜单链接url不能为空";
            }
        }

        //上级菜单类型
        int parentType = Constant.MenuType.CATALOG.getValue();  //枚举类型的字段

        if(menu.getParentId() != 0 ){
            SysMenuEntity parentEntity = sysMenuService.getById(menu.getParentId());
            parentType = parentEntity.getType();
        }

        //目录，菜单
        if(menu.getType() == Constant.MenuType.CATALOG.getValue() || menu.getType() == Constant.MenuType.MENU.getValue()){

            if(parentType !=Constant.MenuType.CATALOG.getValue()){
                return "上级菜单只能为目录类型";
            }
            return "";
        }

        //按钮
        if(menu.getType() == Constant.MenuType.BUTTON.getValue()){

            if(parentType != Constant.MenuType.MENU.getValue()){
                return "上级菜单只能为菜单类型！";
            }
            return "";
        }
        return "";

    }


}