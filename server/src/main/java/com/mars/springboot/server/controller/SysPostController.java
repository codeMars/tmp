package com.mars.springboot.server.controller;


/*
* 岗位管理
* */

import com.google.common.collect.Maps;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.common.utils.ValidatorUtil;
import com.mars.springboot.model.entity.SysPostEntity;
import com.mars.springboot.server.service.SysPostService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Map;

@RestController
@RequestMapping("/sys/post")
public class SysPostController extends AbstractController{

    @Autowired
    private SysPostService sysPostService;

    //分页列表模糊查询
    @GetMapping("/list")
    public BaseResponse list(@RequestParam Map<String,Object> paraMap){


        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();
        try{
            PageUtil page = sysPostService.queryPage(paraMap);
            resMap.put("page",page);

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail);
        }

        response.setData(resMap);
        return response;
    }

    //新增
    //借助validation的验证框架
    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse save(@RequestBody @Validated SysPostEntity entity, BindingResult result){

//        if(StringUtils.isBlank(entity.getPostCode())||StringUtils.isBlank(entity.getPostName())){
//            return new BaseResponse(StatusCode.PostCodeHasExist);
//        }

        String res = ValidatorUtil.checkResult(result);
        if(StringUtils.isNotBlank(res)){
            return new BaseResponse(StatusCode.InvalidParams.getCode(),res);
        }


        BaseResponse response = new BaseResponse(StatusCode.Success);
        logger.info("新增岗位,接收到数据:{}",entity);
        try{
            sysPostService.savePost(entity);

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }


    //获取详情
    @GetMapping(value = "/info/{id}")
    public BaseResponse save(@PathVariable Long id){

        if(id == null || id <=0){
            return new BaseResponse(StatusCode.InvalidParams);
        }
        Map<String,Object> resMap = Maps.newHashMap();
        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("获取详情,接收到数据:{}",id);

            resMap.put("post",sysPostService.getById(id));

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }

    //修改
    @PostMapping(value = "/update",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse update(@RequestBody @Validated SysPostEntity entity, BindingResult result){


        String res = ValidatorUtil.checkResult(result);
        if(StringUtils.isNotBlank(res)){
            return new BaseResponse(StatusCode.InvalidParams.getCode(),res);
        }
        if(entity.getPostId() == null || entity.getPostId() <=0){
            return new BaseResponse(StatusCode.InvalidParams);
        }


        BaseResponse response = new BaseResponse(StatusCode.Success);
        logger.info("修改岗位,接收到数据:{}",entity);
        try{
            sysPostService.updatePost(entity);

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //删除
    @PostMapping(value = "/delete",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse delete(@RequestBody  Long[] ids){


        BaseResponse response = new BaseResponse(StatusCode.Success);
        logger.info("删除岗位,接收到数据:{}", Arrays.asList(ids));
        try{
            sysPostService.deletePach(ids);

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //查询岗位列表
    @GetMapping(value = "/select")
    public BaseResponse select( ){

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("查询岗位列表。。。。");
            Map<String,Object> resMap = Maps.newHashMap();
            resMap.put("list",sysPostService.list());
            response.setData(resMap);

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage( ));
        }
        return response;
    }

}
