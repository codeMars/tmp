package com.mars.springboot.server.controller;

import com.google.common.collect.Maps;
import com.mars.springboot.common.response.BaseResponse;
import com.mars.springboot.common.response.StatusCode;
import com.mars.springboot.common.utils.PageUtil;
import com.mars.springboot.common.utils.ValidatorUtil;
import com.mars.springboot.model.entity.SysRoleEntity;
import com.mars.springboot.server.service.SysRoleDeptService;
import com.mars.springboot.server.service.SysRoleMenuService;
import com.mars.springboot.server.service.SysRoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Autowired
    private SysRoleDeptService sysRoleDeptService;


    //分页列表模糊查询
    @RequestMapping("/list")
    public BaseResponse list(@RequestParam Map<String,Object> paraMap){
        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();
        try{
            PageUtil page =sysRoleService.queryPage(paraMap);
            resMap.put("page",page);
        }catch (Exception e){
            response = new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }

    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse save(@RequestBody @Validated SysRoleEntity entity, BindingResult result){

        String res = ValidatorUtil.checkResult(result);//校验数据
        if(StringUtils.isNotBlank(res)){
            new BaseResponse(StatusCode.InvalidParams.getCode(),res);
        }

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("save操作：{}",entity);
            sysRoleService.saveRole(entity);

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //获取role详情
    @GetMapping(value = "/info/{id}")
    public BaseResponse info(@PathVariable Long id){

        if(id <=0 || id == null){
            return new BaseResponse(StatusCode.InvalidParams);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);
        Map<String,Object> resMap = Maps.newHashMap();

        try{
            logger.info("info操作：{}",id);
            SysRoleEntity roleEntity = sysRoleService.getById(id);

            //获取角色对应的菜单列表
            List<Long> menuIds = sysRoleMenuService.queryMenuList(id);
            roleEntity.setMenuIdList(menuIds);

            //获取角色对应的部门列表
            List<Long> deptIds = sysRoleDeptService.queryDeptIdList(id);
            roleEntity.setDeptIdList(deptIds);

            resMap.put("role",roleEntity);

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }

    @PostMapping(value = "/update",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BaseResponse update(@RequestBody @Validated SysRoleEntity entity, BindingResult result){

        String res = ValidatorUtil.checkResult(result);//校验数据
        if(StringUtils.isNotBlank(res)){
            new BaseResponse(StatusCode.InvalidParams.getCode(),res);
        }

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("update操作：{}",entity);
            sysRoleService.updateOrUpdate(entity);

        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //删除角色
    @PostMapping(value = "/delete",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)  //接收是json格式
    public BaseResponse delete(@RequestBody Long[] ids){

        if(ids == null || ids.length <= 0){
            return new BaseResponse(StatusCode.InvalidParams);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("删除角色-接收数据:{}",ids);
            sysRoleService.deletebBathch(ids);
        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }

        return response;
    }


    //查询角色列表
    @GetMapping(value = "/select")
    public BaseResponse select( ){

        BaseResponse response = new BaseResponse(StatusCode.Success);
        try{
            logger.info("查询角色-接收数据");
             Map<String,Object> resMap = Maps.newHashMap();
             resMap.put("list",sysRoleService.list());
             response.setData(resMap);
        }catch (Exception e){
            return new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }



}
